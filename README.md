# Testovacie zadanie
V súbore `src/Handler` sú umiestnené 3 handlery, ktoré sú zodpovedné za spracovanie
serverových requestov:
* `InsertProductHandler.php` - slúži na pridanie nového produktu.
* `ListAllProductsHandler.php` - slúži na vylistovanie všetkých produktov.
* `ListCategoryProductsHandler.php` - slúži na vylistovanie produktov pre danú kategóriu.

V súbore `public` sú umiestnené jednotlivé front controllery pre spomínané handlery.

## Entita
Kód pracuje s dátami, ktoré reprezentujú produkt. Produkt sa skladá z dvoch fieldov:
* `name` - meno produktu (string).
* `categoryId` - ID kategórie, kam produkt spadá (integer).

## Zadanie
Naprogramujte a upravte existujúci kód tak, aby bolo možné:
1. Pridať nový produkt.
2. Vylistovať si všetky produkty.
3. Vylistovať si produkty spadajúcich pod danú kategóriu.

### Bussiness pravidlá
* Kategória môže obsahovať maximálne 10 produktov.

### Technologické pravidlá
* Zadanie obsahuje dve úlohy:
    1. Upratanie existujúceho kódu podľa podmienok zadania. Všetky tri handlery sú naimplementované rámcovo.
    2. Naprogramovanie chýbajúcej časti zadania.
* Výsledný kód má byť prehľadný, dobre rozdelený a testovateľný.
Pri riešení zadania je možné (a žiadúce) využiť:
    1. SOLID princípy
    2. návrhové vzory
    3. akékoľvek princípy návrhu a vývoja, ktoré pomôžu docieliť čistý a prehľadný kód.
* Dev stack:
    1. PHP 7.4
* Zoznam nainštalovaných balíčkov:
    1. [nyholm/psr7](https://github.com/Nyholm/psr7)
    2. [nyholm/psr7-server](https://github.com/Nyholm/psr7-server)
    3. [psr/http-server-handler](https://github.com/php-fig/http-server-handler)
    4. [psr/http-server-middleware](https://github.com/php-fig/http-server-middleware)
    5. [laminas/laminas-httphandlerrunner](https://github.com/laminas/laminas-httphandlerrunner)
* Pri riešení nepoužívajte knižnice, ani časti frameworkov, zoznam nainštalovaných balíčkov by ste nemali rozširovať (v prípade dodatočnej inštalácie si dôvod treba obhájiť).
* Ďalšie pravidlá:
    1. Dodržiavať signatúry metód.
    2. Dodržiavať striktnú typovosť.
    3. Nahradiť v kóde "PHP hacky" - exit.
    
**Disclaimer**:
Zadanie je umelé, výsledkom nemúsí byť spustiteľný kód.
Kvôli časovej náročnosti môžu byť implementačné detaily abstrahované (technologická implementácia ukladania).
Ukladanie môže byť realizované cez JSON súbor, prípade len TODO blokom.
Ak by bol však kód aj reálne spustiteľný, je to plus.

## Spustenie
Zadanie je postavené na dockeri.
Na spustenie sa vyžaduje Docker desktop.
Následne sa dá cez kontajner nainštalovať závislovsti cez composer, ktorý je predinštalovaný. 
## Inštalácia
Pre zjednodušenú interakciu s dockerom je v zadaní pridaný skript `docker/ops`.
Spustenie kontajnera:
```bash
./ops up -d
```
Otvorenie bash shell v kontajneri:
```bash
./ops container bash
```
Následne je možná interakcia s kontajnerom ako s bežným linuxovým shellom.
Inštalácia:
```bash
composer install
```
Skratke pre inštaláciu spustenú z host zariadenia:
```bash
./ops bash composer install
```
