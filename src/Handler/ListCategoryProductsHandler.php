<?php

declare(strict_types=1);

namespace TestAssignment\Handler;


use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ListCategoryProductsHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if (!$request->hasHeader('Authorization')) {
            echo '400: Bad response 1';
            exit;
        }

        list(, $credentials) = explode(' ', current($request->getHeader('Authorization')));
        $decodedCredentials = base64_decode($credentials);
        list($username, $password) = explode(':', $decodedCredentials);
        if (!$this->verifyAdmin($username, $password)) {
            echo '401: Bad response 3';
            exit;
        }

        $queryParams = $request->getQueryParams();
        if (!array_key_exists('categoryId', $queryParams)) {
            echo '400: Bad response 4';
            exit;
        }

        $factory  = new Psr17Factory();
        $response = $factory->createResponse(200, 'OK');

        $format = $request->getQueryParams()['format'];
        if (!in_array($format, ['xml', 'json', 'text'])) {
            echo '400: Bad response 5';
            exit;
        }

        $contentTypes = [
            'json' => 'application/json',
            'xml'  => 'text/xml',
            'text' => 'text/plain'
        ];

        $response   = $response->withAddedHeader('Content-Type', $contentTypes[$format]);
        $categoryId = $queryParams['categoryId'];
        $data       = [];

        //@todo fetch products by $categoryId
        /*$data     = [
            1 => [
                'name' => 'Product 1'
            ],
            2 => [
                'name' => 'Product 2'
            ],
            3 => [
                'name' => 'Product 3'
            ]
        ];*/

        $response = $response->withBody(
            Stream::create(
                static::formatArrayOutput($data, $format)
            )
        );

        return $response;
    }

    private function verifyAdmin(string $username, string $password): bool
    {
        //same function as other `verifyAdmin` functions
        return true;
    }

    private static function formatArrayOutput(array $rawOutput, string $format): string
    {
        if ($format === 'json') {
            return 'JSON representation of output data; List2Handler';
        }

        if ($format === 'xml') {
            return 'XML representation of output data; List2Handler';
        }

        if ($format === 'text') {
            return 'Text representation of output data; List2Handler';
        }

        echo '400: Bad response 6';
        exit;
    }
}