<?php

declare(strict_types=1);

namespace TestAssignment\Handler;

use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class InsertProductHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $body = $request->getParsedBody();
        if (!array_key_exists('csrf-token', $body)) {
            echo '400: Bad response 1';
            exit;
        }

        if (!static::verifyCsrfToken($body['csrf-token'])) {
            echo '403: Bad response 2';
            exit;
        }

        if (!static::verifyProductData($body)) {
            echo '401: Bad response 3';
            exit;
        }

        //@todo store new product

        $factory = new Psr17Factory();

        return $factory->createResponse(201, 'Created');
    }

    private static function verifyCsrfToken(string $token): bool
    {
        return true;
    }

    private static function verifyProductData(array $rawData): bool
    {
        if (!array_key_exists('product', $rawData)) {
            return false;
        }

        //...

        return true;
    }
}