<?php

declare(strict_types=1);

use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;
use TestAssignment\Handler\ListCategoryProductsHandler;

require __DIR__ . '/../vendor/autoload.php';

$psr17Factory = new Psr17Factory();

$creator = new ServerRequestCreator(
    $psr17Factory,
    $psr17Factory,
    $psr17Factory,
    $psr17Factory
);

$request = $creator->fromGlobals();
$handler = new ListCategoryProductsHandler();
$emitter = new SapiEmitter();

$emitter->emit(
    $handler->handle($request)
);